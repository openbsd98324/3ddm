

//////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>      //strlen
#include <unistd.h>   //close 
#include <sys/socket.h>  //socket
#include <arpa/inet.h>   //inet_addr
#include <sys/socket.h> 
#include <arpa/inet.h>    //close 
#include <netinet/in.h> 
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros 
#include <errno.h> 
#include <unistd.h>   //close 
#include <arpa/inet.h>    //close 

#include <termios.h>
#include <time.h>
    
#define TRUE   1 
#define FALSE  0 
#define PORT 7777 


#define ESC "\033"
#define gotoxy(y,x)		printf(ESC "[%d;%dH", y, x);


int debug_mode = 0; 
int view_mode  = 0; 
int score[5];


void draw( char *str )
{
      int x, y ; 
      char ptr[ 2 * strlen(str)+1];
      int i,j,info=1; 
      int infocell[ 20 ]; 
      ptr[ 0 ] = '\0'; info = 1;  
      strncpy( ptr , "", strlen( ptr ) );

      j = 0;   
      for(i=0; str[i]!='\0'; i++)
      {
        if ( str[i] == ';' )
        {
           if ( debug_mode == 1 ) 
              printf( "-%s-\n", ptr );
           infocell[ info ] = atoi( ptr ); 
           ptr[ 0 ] = '\0'; j = 0; 
           info++;
        }
        else 
        if (str[i] != '\n' && str[i] != '\n') 
           ptr[j++]=str[i];
      } 
      ptr[j]='\0';


     printf( ESC "[2J" );
     printf( ESC "[H" );

     if ( view_mode  == 0 )  
     {
       printf( ESC "[H" );
       y = infocell[ 1 ];
       x = infocell[ 2 ];
       gotoxy(y,x); 
       printf( "1" );

       printf( ESC "[H" );
       y = infocell[ 1+4 ];
       x = infocell[ 2+4 ];
       gotoxy(y,x); 
       printf( "2" );

       printf( ESC "[H" );
       y = infocell[ 1+4*2 ];
       x = infocell[ 2+4*2 ];
       gotoxy(y,x); 
       printf( "M" );
     }

     if ( debug_mode == 1 ) 
     {
       printf( ESC "[H" );
       printf( "%s", str );
       gotoxy( 2,1); 
       for( j = 1 ; j <= 3 * 4 ; j++)
         printf( "%d ", infocell[ j ] );
     }

      gotoxy( 0, 0); 
      printf( "  |Score: %d|%d|%d|", score[1], score[2] , score[3] ); 

      if (( infocell[ 1 ] == infocell[ 1+4*2 ]) && ( infocell[ 2 ] == infocell[ 2+4*2 ])) 
      {
          gotoxy(   0, 0); 
          printf(  "  MONSTER CATCH!" );
          score[ 1 ]--;
          score[ 3 ]++;
      }
      if  (( infocell[ 1+4 ] == infocell[ 1+4*2 ]) && ( infocell[ 2+4 ] == infocell[ 2+4*2 ])) 
      {
          gotoxy(   0, 0); 
          score[ 2 ]--;
          score[ 3 ]++;
          printf(  "  MONSTER CATCH!" );
      }


       gotoxy( 0, 0); 
       printf( " " );
}




//////////////////////////////////////////
//////////////////////////////////////////
int main( int argc, char *argv[])
{

       score[1] = 0;
       score[2] = 0;
       score[3] = 0;

       struct sockaddr_in server;
       server.sin_addr.s_addr = inet_addr( "38.87.162.23" );
       server.sin_family = AF_INET;
       server.sin_port = htons( 7777 );

       int sock = socket(AF_INET , SOCK_STREAM , 0);
       connect( sock , (struct sockaddr *)&server , sizeof(server));

       struct termios ot;
       if(tcgetattr(STDIN_FILENO, &ot) == -1) perror(")-");
       struct termios t = ot;
       t.c_lflag &= ~(ECHO | ICANON);
       t.c_cc[VMIN] = 1;
       t.c_cc[VTIME] = 0;
       if(tcsetattr(STDIN_FILENO, TCSANOW, &t) == -1) perror(")-");

       int draw_mode = 1;

       int a; char sockout[ 5 ] ;
       while( a = getchar() , a != 'Q') 
       {
            if ( ( ( a >= 'a' )  && ( a <= 'z' ))
            || ( ( a >= 'A' )  && ( a <= 'Z' ))
            || ( ( a >= '1' )  && ( a <= '9' ))
            || ( a == '\n' ) 
            || ( a == '^' ) 
            || ( a == '[' ) 
            || ( a == ']' ) 
            || ( a == ',' ) 
            || ( a == ';' ) 
            || ( a == '.' ) 
            || ( a == '0' ) 
            || ( a == 27 ) 
            || ( a == 21 )  // ^+u
            || ( a == 4 )   // ^+d
            || ( a == 8 ) 
            || ( a == ' ' ) )
            {
                   if ( a == 'v' ) 
                   {
                       if ( view_mode == 0 ) 
                          view_mode = 1;
                       else if ( view_mode == 1 ) 
                          view_mode = 2;
                       else if ( view_mode == 2 ) 
                          view_mode = 3;
                       else if ( view_mode == 3 ) 
                          view_mode = 0;
                   }



                   sockout[ 0 ] = a;
                   sockout[ 1 ] = '\0';
                   send( sock , sockout , 1 , 0);

                   char buffer[1025]; int valread = 0;  int phr = 0; 
                   // recv( sock       , output , 100 , 0);
		   //recv( sock , buffer , sizeof( buffer ), 0);
                   //Check if it was for closing , and also read the                    
                   //incoming message                                                   
           
                //for( phr = 1 ; phr <= 2 ; phr++ )
                   valread = read( sock , buffer, 1024);
                         buffer[valread] = '\0';                                          
                         int foi;
                         for( foi = 0; foi < strlen( buffer ); foi++ )
                         {
                           if( buffer[foi] == '\n') 
                            buffer[foi] = '\0';
                         }

                     printf( " " );

                     if ( debug_mode == 1 ) 
                      printf( "  BUFFER: %d |%s| [%d]\n" , sock , buffer , buffer[ 0 ] );

                if ( draw_mode == 1 )  
                    draw( buffer );

            }
        }


        // clean up
        if(tcsetattr(STDIN_FILENO, TCSANOW, &ot) == -1) perror(")-");
        close(sock);
        return 0;
}




