

   #include <stdio.h>                                                                   
   #include <string.h> 
   #include <stdlib.h>                                                                  

   #include <errno.h>                                                                   
   #include <unistd.h>      // sleep in there

   #include <arpa/inet.h>    //close                                                    
   #include <sys/types.h>                                                               
   #include <sys/socket.h>                                                              
   #include <netinet/in.h>                                                              
   #include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros                             
                                                                                        
   #define TRUE   1                                                                     
   #define FALSE  0                                                                     
   #define PORT 8888                                                                    

int player1_y = 5; 
int player1_x = 5; 
int player1_o = 1; 
int player1_a = 0; 

int player2_y = 8; 
int player2_x = 8; 
int player2_o = 1; 
int player2_a = 0; 

int player3_y = 18; 
int player3_x = 18; 
int player3_o = 1; 
int player3_a = 0; 


#define BUFMAX 1025
char msgout[BUFMAX]; 
                                                                                        
int main(int argc , char *argv[])                                                    
{                                                                                    
     char linestr[BUFMAX];

       int opt = TRUE;                                                                  
       int master_socket , addrlen , new_socket , client_socket[30] ,                   
             max_clients = 30 , activity, i , valread , sd;                             
       int max_sd;                                                                      
       struct sockaddr_in address;                                                      
                                                                                        
       char buffer[BUFMAX];  //data buffer of 1K                                          
                                                                                        
       //set of socket descriptors                                                      
       fd_set readfds;                                                                  
                                                                                        
       //a message                                                                      
       char *message = "ECHO Daemon v1.0 \r\n";                                         
                                                                                        
       //initialise all client_socket[] to 0 so not checked                             
       for (i = 0; i < max_clients; i++)                                                
       {                                                                                
           client_socket[i] = 0;                                                        
       }                                                                                
                                                                                        
       //create a master socket                                                         
       if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0)                    
       {                                                                                
           perror("socket failed");                                                     
           exit(EXIT_FAILURE);                                                          
       }                                                                                
                                                                                        
       //set master socket to allow multiple connections ,                              
       //this is just a good habit, it will work without this                           
       if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,            
             sizeof(opt)) < 0 )                                                         
       {                                                                                
           perror("setsockopt");                                                        
           exit(EXIT_FAILURE);                                                          
       }                                                                                
                                                                                        
       //type of socket created                                                         
       address.sin_family = AF_INET;                                                    
       address.sin_addr.s_addr = INADDR_ANY;                                            
       address.sin_port = htons( PORT );                                                
                                                                                        
       //bind the socket on port 8888                                         
       if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0)         
       {                                                                                
           perror("bind failed");                                                       
           exit(EXIT_FAILURE);                                                          
       }                                                                                
       printf("Listener on port %d \n", PORT);                                          
                                                                                        
       //try to specify maximum of 3 pending connections for the master socket          
       if (listen(master_socket, 3) < 0)                                                
       {                                                                                
           perror("listen");                                                            
           exit(EXIT_FAILURE);                                                          
       }                                                                                
                                                                                        
       //accept the incoming connection                                                 
       addrlen = sizeof(address);                                                       
       puts("Waiting for connections ...");                                             
                                                                                        
       while(TRUE)                                                                      
       {                                                                                
           //clear the socket set                                                       
           FD_ZERO(&readfds);                                                           
                                                                                        
           //add master socket to set                                                   
           FD_SET(master_socket, &readfds);                                             
           max_sd = master_socket;                                                      
                                                                                        
           //add child sockets to set                                                   
           for ( i = 0 ; i < max_clients ; i++)                                         
           {                                                                            
               //socket descriptor                                                      
               sd = client_socket[i];                                                   
                                                                                        
               //if valid socket descriptor then add to read list                       
               if(sd > 0)                                                               
                   FD_SET( sd , &readfds);                                              
                                                                                        
               //highest file descriptor number, need it for the select function        
               if(sd > max_sd)                                                          
                   max_sd = sd;                                                         
           }                                                                            
                                                                                        
           //wait for an activity on one of the sockets , timeout is NULL ,             
           //so wait indefinitely                                                       
           activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);              
                                                                                        
           if ((activity < 0) && (errno!=EINTR))                                        
           {                                                                            
               printf("select error");                                                  
           }                                                                            
                                                                                        
           //If something happened on the master socket ,                               
           //then its an incoming connection                                            
           if (FD_ISSET(master_socket, &readfds))                                       
           {                                                                            
               if ((new_socket = accept(master_socket,                                  
                       (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)           
               {                                                                        
                   perror("accept");                                                    
                   exit(EXIT_FAILURE);                                                  
               }                                                                        
                                                                                        
               //inform user of socket number - used in send and receive commands       
               printf("New connection , socket fd is %d , ip is : %s , port : %d\n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));                                               
                                                                                        
               //send new connection greeting message                                   
               if( send(new_socket, message, strlen(message), 0) != strlen(message) )   
               {                                                                        
                   perror("send");                                                      
               }                                                                        
                                                                                        
               puts("Welcome message sent successfully");                               
                                                                                        
               //add new socket to array of sockets                                     
               for (i = 0; i < max_clients; i++)                                        
               {                                                                        
                   //if position is empty                                               
                   if( client_socket[i] == 0 )                                          
                   {                                                                    
                       client_socket[i] = new_socket;                                   
                       printf("Adding to list of sockets as %d\n" , i);                 

                       printf("Socketmax = %d\n", max_clients );
                       break;                                                           
                   }                                                                    
               }                                                                        
           }                                                                            

                                                                                        
           //else its some IO operation on some other socket                            
           for (i = 0; i < max_clients; i++)                                            
           {                                                                            
               sd = client_socket[i];                                                   
                                                                                        
               if (FD_ISSET( sd , &readfds))                                            
               {                                                                        
                   //Check if it was for closing , and also read the                    
                   //incoming message                                                   
                   if ((valread = read( sd , buffer, 1024)) == 0)                       
                   {                                                                    
                       //Somebody disconnected , get his details and print              
                       getpeername(sd , (struct sockaddr*)&address , (socklen_t*)&addrlen);                                       
                       printf("Host disconnected , ip %s , port %d \n" ,                
                             inet_ntoa(address.sin_addr) , ntohs(address.sin_port));    
                       printf("Socketmax = %d\n", max_clients );
                                                                                        
                       //Close the socket and mark as 0 in list for reuse               
                       close( sd );                                                     
                       client_socket[i] = 0;                                            
                   }                                                                    
                                                                                        
                   //Echo back the message that came in                                 
                   else                                                                 
                   {                                                                    
                         buffer[valread] = '\0';                                          
                         int foi;
                         for( foi = 0; foi < strlen( buffer ); foi++ )
                         {
                           if( buffer[foi] == '\n') 
                            buffer[foi] = '\0';
                         }
                         printf( "  BUFFER: %d | %s|\n" , sd , buffer );

                         if     ( buffer[0] == 'j')  player1_y++;
                         else if( buffer[0] == 'k')  player1_y--;
                         else if( buffer[0] == 'l')  player1_x++;
                         else if( buffer[0] == 'h')  player1_x--;

                         else if ( buffer[0] == 'w')  player2_y--;
                         else if ( buffer[0] == 's')  player2_y++;
                         else if ( buffer[0] == 'a')  player2_x--;
                         else if ( buffer[0] == 'd')  player2_x++;

                         else if ( buffer[0] == '5')  player3_y--;
                         else if ( buffer[0] == '2')  player3_y++;
                         else if ( buffer[0] == '1')  player3_x--;
                         else if ( buffer[0] == '3')  player3_x++;

                         else if ( buffer[0] == '0')
{
 player1_y = 5; 
 player1_x = 5; 
 player1_o = 1; 
 player1_a = 0; 

 player2_y = 8; 
 player2_x = 8; 
 player2_o = 1; 
 player2_a = 0; 

 player3_y = 18; 
 player3_x = 18; 
 player3_o = 1; 
 player3_a = 0; 
  }


                         //send( client_socket[ i ] , "HelloWorld" , strlen( "HelloWorld" ) , 0 );                         
                         snprintf( msgout , sizeof( msgout ), "%002d;%002d;%002d;%002d;%002d;%002d;%002d;%002d;%002d;%002d;%002d;%002d",  player1_y , player1_x, player1_o, player1_a ,
                          player2_y , player2_x, player2_o, player2_a ,
                          player3_y , player3_x, player3_o, player3_a );

                         send( client_socket[ i ] , msgout , strlen( msgout ) , 0 );                         


                         int k ;  int userk = sd ; 
                         for (k = 0; k < max_clients; k++)                                            
                         {                                                                            
                            if (FD_ISSET( client_socket[ k ]  , &readfds))                                            
                            {  } 
                            else 
                            {
                               //set the string terminating NULL byte on the end                
                               //of the data read                                               
                               snprintf( msgout ,  sizeof( msgout ), "%d (Socket %d/%d/%d/%s): %s\n", (int)time(NULL), userk ,  client_socket[ k ] , max_clients,   " " ,  buffer );

                               send( client_socket[ k ] , msgout , strlen( msgout ) , 0 );                         

                            }
                          }
                   }                                                                    
               }                                                                        
           }                                                                            
       }                                                                                
                                                                                        
       return 0;                                                                        
 }                                                                                    



/*
server.c
   filter_none
   edit
   close

   play_arrow

   link
   brightness_4
   code

   // Server side C/C++ program to demonstrate Socket programming         
   #include <unistd.h>                                                    
   #include <stdio.h>                                                     
   #include <sys/socket.h>                                                
   #include <stdlib.h>                                                    
   #include <netinet/in.h>                                                
   #include <string.h>                                                    
   #define PORT 8080                                                      
   int main(int argc, char const *argv[])                                 
   {                                                                      
       int server_fd, new_socket, valread;                                
       struct sockaddr_in address;                                        
       int opt = 1;                                                       
       int addrlen = sizeof(address);                                     
       char buffer[1024] = {0};                                           
       char *hello = "Hello from server";                                 
                                                                          
       // Creating socket file descriptor                                 
       if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)            
       {                                                                  
           perror("socket failed");                                       
           exit(EXIT_FAILURE);                                            
       }                                                                  
                                                                          
       // Forcefully attaching socket to the port 8080                    
       if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                     &opt, sizeof(opt)))  
       {                                                                  
           perror("setsockopt");                                          
           exit(EXIT_FAILURE);                                            
       }                                                                  
       address.sin_family = AF_INET;                                      
       address.sin_addr.s_addr = INADDR_ANY;                              
       address.sin_port = htons( PORT );                                  
                                                                          
       // Forcefully attaching socket to the port 8080                    
       if (bind(server_fd, (struct sockaddr *)&address,                   
                                    sizeof(address))<0)                   
       {                                                                  
           perror("bind failed");                                         
           exit(EXIT_FAILURE);                                            
       }                                                                  
       if (listen(server_fd, 3) < 0)                                      
       {                                                                  
           perror("listen");                                              
           exit(EXIT_FAILURE);                                            
       }                                                                  
       if ((new_socket = accept(server_fd, (struct sockaddr *)&address,   
                          (socklen_t*)&addrlen))<0)                       
       {                                                                  
           perror("accept");                                              
           exit(EXIT_FAILURE);                                            
       }                                                                  
       valread = read( new_socket , buffer, 1024);                        
       printf("%s\n",buffer );                                            
       send(new_socket , hello , strlen(hello) , 0 );                     
       printf("Hello message sent\n");                                    
       return 0;                                                          
   }                                                                      
*/



